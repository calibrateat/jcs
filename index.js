// --------------------------------------------- //
//                                               //
//     JCS - JSON Condition Service              //
//                  by calibrate.at              //
//                                               //
//    setting    (see condition object)          //
//       'onlyFirst': false,                     //
//       'first2last': true,                     //
//       'placeholder': false,                   //
//       'stepResolver': false,                  //
//       'debug': false                          //
//                                               //
// --------------------------------------------- //
const Inherit = require('object-inheritance')

/* const inheritSetting = {
  'id': 'id, key',
  'arrayExtend': 'end'
} */

const util = require('./lib/util')
const mingo = require('mingo')
const refResolver = require('referenz-resolver')

mingo.setup({
  'key': 'id'
})

/**
 * Iterate over conditions and response then node
 * @param {object} conditionObj
 * @param {object} dataObj
 * @param {boolean} firstElement
 */
function jcs (conditionObj, dataObj) {
  // setting
  let _setting = this.setting = readSetting(conditionObj.setting)
  if (_setting.debug) {
    logMessage('--- Setting: ––––––––––-')
    logMessage(JSON.stringify(_setting))
  }
  // conditons
  let _conditions = conditionObj.conditions
  if (!(_conditions instanceof Array)) {
    throw new Error('type error in condition object (must be a array)')
  }
  // data
  let _data = dataObj || {}
  if (conditionObj.data) { // inherit for calc extension
    _data = Object.assign(_data, conditionObj.data) // ToDo deep copy
  }
  if (typeof _data !== 'object') {
    throw new Error('type error in condition data object')
  }
  // global => result
  let _result = conditionObj.global || {}
  // check conditions length
  if (!_conditions.length) {
    return _result
  } else {
    _data = Object.defineProperty(_data, '__result', {
      value: _result,
      writable: true
    })
  }
  if (_data.hasOwnProperty('__calc')) {
    _setting.placeholder = true
    let __calcResolved = refResolver.call(this, _data['__calc'], _data)
    for (let _newValue in __calcResolved) {
      let _structure = __calcResolved[_newValue]
      // check '$if' construct
      if (_structure.hasOwnProperty('$if')) {
        let _ifQuery = new mingo.Query(_structure['$if'])
        let _ifResult = _ifQuery.test(_data)
        let _targetValue = null
        if (_ifResult) {
          if (_structure.hasOwnProperty('$then')) {
            _targetValue = _structure['$then']
          }
        } else {
          if (_structure.hasOwnProperty('$else')) {
            _targetValue = _structure['$else']
          }
        }
        __calcResolved[_newValue] = _targetValue
      }
    }
    if (_setting.debug) {
      logMessage('--- __calc is now: ––––––––––-')
      logMessage(JSON.stringify(__calcResolved))
    }
    _data['__calc'] = __calcResolved
  }

  if (!_setting.first2last) {
    _conditions.reverse()
  }
  if (_setting.debug) {
    logMessage('--- Conditions are: ––––––––––-')
    logMessage(JSON.stringify(_conditions))
  }
  let _completeResolved = false
  if (_setting.placeholder) {
    if (_data == null || util.isEmpty(_data)) {
      throw new Error('no data for replacing')
    }
    // condition resolving
    if (!_setting.stepResolver) {
      _conditions = refResolver.call(this, _conditions, _data)
      _completeResolved = true
    }
    /* else {
         // ToDo testing
         _conditions[0] = refResolver.call(this, _conditions[0], _data)
       } */
  }
  // set sort
  if (_setting.debug && _completeResolved) {
    logMessage('--- Conditions complete reolved ––––––––––-')
    logMessage(JSON.stringify(_conditions))
  }
  for (let _conIndex = 0, _conLength = _conditions.length; _conIndex < _conLength; _conIndex++) {
    let _condition = _conditions[_conIndex]
    if (!_completeResolved && _setting.stepResolver) {
      console.log(JSON.stringify(_condition))
      _condition = refResolver.call(this, _condition, _data)
      if (_setting.debug) {
        let _comp = (_completeResolved) ? 'complied' : 'not complied'
        logMessage('--- Conditions ' + _comp + ' reolved current conditon now: ––––––––––-')
        logMessage(JSON.stringify(_condition))
      }
    }
    let _conditionResult = {}
    let _isSubCondition = false
    if (_condition.hasOwnProperty('subCondition')) {
      _isSubCondition = Boolean(_condition['subCondition'])
    }
    if (!_isSubCondition) {
      _conditionResult = checkCondition.call(this, _condition, _data, _setting)
    }
    while (_conditionResult.hasOwnProperty('$goToId')) {
      let _subResult = {}
      let _conditionId = _conditionResult['$goToId']
      let _conditionIndex
      let _goCondition = _conditions.find((_element, _index) => {
        if (_element.id === _conditionId) {
          _conditionIndex = _index
          return _element
        }
      })
      if (_goCondition) {
        // not strict solution
        if (!_goCondition['$goBack']) {
          _conIndex = _conditionIndex
        }
        _conIndex = _conditionIndex
        if (!_completeResolved && _setting.stepResolver) {
          refResolver.call(this, _goCondition, _data)
        }
        _subResult = checkCondition.call(this, _goCondition, _data, _setting)
        if (!_subResult.hasOwnProperty('$goToId')) {
          delete _conditionResult['$goToId']
        }
        // _conditionResult = util.updateObject(_conditionResult, _subResult)
        let inherit = new Inherit()
        inherit.setting = {
          'id': 'key, id'
        }
        _conditionResult = inherit.start(_conditionResult, _subResult)
      } else {
        delete _conditionResult['$goToId']
      }
    }
    if (!util.isEmpty(_conditionResult)) {
      // _data.__result = util.updateObject(_data.__result, _conditionResult)
      let inherit = new Inherit()
      inherit.setting = {
        'id': 'key, id'
      }
      _data.__result = inherit.start(_data.__result, _conditionResult)
    }
    if (_setting.debug) {
      logMessage('--- Result is now: ––––––––––-')
      logMessage(JSON.stringify(_data.__result))
    }
    if (_conditionResult.hasOwnProperty('$break')) {
      let _break = _conditionResult['$break']
      delete _data.__result['$break']
      if (_break) {
        break
      }
    }
    if (_conditionResult && _setting.onlyFirst) {
      break
    }
  }
  if (_setting.debug) {
    logMessage('--- end result is: ––––––––––-')
    logMessage(JSON.stringify(_data.__result))
  }
  if (_setting.append) {
    let _target = _data.__result
    let _source = _data
    if (_source.hasOwnProperty('__calc')) {
      delete _source['__calc']
    }
    if (_target.hasOwnProperty('__ignore')) {
      delete _target['__ignore']
    }
    // _data.__result = util.updateObject(_source, _target)
    let inherit = new Inherit()
    inherit.setting = {
      'id': 'key, id'
    }
    _data.__result = inherit.start(_source, _target)
  }
  return _data.__result
}
/**
 *
 *
 * @param {object} condition
 * @param {object} dataObj
 * @returns {object}
 */
function checkCondition (condition, dataObj, setting) {
  let _setting = setting || {}
  let _result = {}
  let _mQuery = new mingo.Query(condition.query)
  // let _breakCall = false
  let _key = 'else'
  let _match = _mQuery.test(dataObj)
  if (_match) {
    _key = 'then'
  }
  _result = condition[_key] || {}
  if (_setting.debug) {
    logMessage('--- Condition ' + condition.id + ' has this result: ––––––––––-')
    logMessage(JSON.stringify(_result))
  }
  if (_result.hasOwnProperty('$log')) {
    logMessage(_result['$log'])
    delete _result['$log']
  }
  return _result
}

/**
 *
 * @param {object} setting
 */
function readSetting (setting) {
  setting = setting || {}
  let _defaults = {
    'onlyFirst': false,
    'first2last': true,
    'placeholder': false,
    'stepResolver': false,
    'append': true,
    'debug': false,
    'digits': 2
  }
  setting = Object.assign(_defaults, setting)
  return setting
}
/**
 * Out a message
 * @param {String} message
 */
function logMessage (message) {
  console.log(message)
}
/**
 * Split Condititons by $setID
 * @param {Array} conditions
 */
/* function conditionSplit (conditions) {
  // let _conArr = [],
  // let _query = {"""$in":}
  // let _mQuery = new mingo.Query(_query)
  // let _match = _mQuery.test(conditions)
  // return _conArr
} */

module.exports = jcs
