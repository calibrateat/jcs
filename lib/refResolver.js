const util = require('./util')

module.exports = refResolver

let setting = {
  'onlyFirst': false,
  'first2last': true,
  'placeholder': false,
  'stepResolver': false,
  'append': true,
  'debug': false,
  'digits': 2
}

/**
   * Replace all "#/" refs with values
   * From dataCollection if given
   * Else from refCollection
   *
   * @param {Object} refObject
   * @param {Object} dataObj
   * @returns {Object}
   */
function refResolver (refObject, dataObj) {
  if (!this.setting) {
    this.setting = setting
  }
  /**
   * Object Calculation
   * sum … Array
   * sub … Array
   * mul … Array
   * div … Array
   * mod … Array
   * abs … Object
   *
   * @param {*} exp
   */
  function calc (exp) {
    if (exp == null || typeof exp !== 'object') {
      return parseFloat(exp)
    }
    let term = Object.keys(exp)[0]
    let _digits = this.setting.digits
    if (exp.hasOwnProperty('$digits')) {
      _digits = exp['$digits']
    }
    let innerExpr = exp[term]
    let calcExp
    switch (term.toLowerCase()) {
      case 'sum':
        if (!(innerExpr instanceof Array)) {
          return null
        } else {
          calcExp = calc(innerExpr[0])
          for (let i = 1, len = innerExpr.length; i < len; i++) {
            calcExp += calc(innerExpr[i])
          }
        }
        break
      case 'sub':
        if (!(innerExpr instanceof Array)) {
          return null
        } else {
          calcExp = calc(innerExpr[0])
          for (let i = 1, len = innerExpr.length; i < len; i++) {
            calcExp -= calc(innerExpr[i])
          }
        }
        break
      case 'mul':
        if (!(innerExpr instanceof Array)) {
          return null
        } else {
          calcExp = calc(innerExpr[0])
          for (let i = 1, len = innerExpr.length; i < len; i++) {
            calcExp *= calc(innerExpr[i])
          }
        }
        break
      case 'div':
        if (!(innerExpr instanceof Array)) {
          return null
        } else {
          calcExp = calc(innerExpr[0])
          for (let i = 1, len = innerExpr.length; i < len; i++) {
            calcExp /= calc(innerExpr[i])
          }
        }
        break
      case 'mod':
        if (!(innerExpr instanceof Array)) {
          return null
        } else {
          calcExp = calc(innerExpr[0])
          for (let i = 1, len = innerExpr.length; i < len; i++) {
            calcExp %= calc(innerExpr[i])
          }
        }
        break
      case 'abs':
        if (innerExpr == null || typeof innerExpr !== 'object') {
          calcExp = Math.abs(innerExpr)
        } else {
          return null
        }
        break
    }
    // console.log('digits: ' + _digits)
    // console.log('value: ' + calcExp)
    // console.log('isFloat: ' + util.isFloat(calcExp))
    // console.log('Rounded is: ' + Number((calcExp).toFixed(_digits)))
    calcExp = Number((calcExp).toFixed(_digits))
    // console.log('isFloat: ' + util.isFloat(calcExp))
    return calcExp
  }
  function refClone (inObject) {
    let resolvedClone
    if (inObject == null || typeof inObject !== 'object') {
      if (typeof inObject === 'string') {
        // console.log(inObject)
        let regRef = new RegExp('^#')
        if (inObject.match(regRef)) {
          let path = inObject.replace(/\//g, '.')
          path = path.replace(/^#./, '')
          path = path.replace(/"$/, '')
          let _nestedString = nestedByString.call(this, dataObj, path)
          if (_nestedString === null || _nestedString === undefined) {
            inObject = null
          } else {
            inObject = _nestedString
          }
        }
      }
      return inObject
    }
    if (inObject instanceof Date) {
      resolvedClone = new Date()
      resolvedClone.setTime(inObject.getTime())
      return resolvedClone
    }
    if (inObject instanceof Array) {
      resolvedClone = []
      for (let i = 0, len = inObject.length; i < len; i++) {
        resolvedClone[i] = refClone.call(this, inObject[i])
      }
      return resolvedClone
    }
    if (inObject instanceof Object) {
      resolvedClone = {}
      for (let attr in inObject) {
        if (inObject.hasOwnProperty(attr)) {
          resolvedClone[attr] = refClone.call(this, inObject[attr])
        }
        if (resolvedClone[attr] instanceof Object) {
          if (resolvedClone[attr].hasOwnProperty('$calc')) {
            resolvedClone[attr] = calc.call(this, resolvedClone[attr]['$calc'])
          }
        }
      }
      return resolvedClone
    }
    throw new Error("Unable to copy obj! Its type isn't supported.")
  }
  return refClone(refObject)
}
/**
 *
 * @param {Object} object
 * @param {String} path
 */
let nestedByString = function (object, path) {
  object = object || {}
  path = path || ''
  path = path.replace(/\[(\w+)\]/g, '.$1')
  path = path.replace(/^\./, '')
  let arr = path.split('.')
  for (let index = 0, n = arr.length; index < n; ++index) {
    let k = arr[index]
    // if (k in object) {
    if (object.hasOwnProperty(k)) {
      object = object[k]
    } else {
      return
    }
  }
  return object
}
