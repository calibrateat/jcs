module.exports = {
  'isEmpty': isEmpty,
  'isInt': isInt,
  'isFloat': isFloat,
  'updateObject': updateObject
}

/**
 * Check if a income object is empty
 * @param {Object} obj
 * @returns {Boolean}
 */
function isEmpty (obj) {
  // null and undefined are "empty"
  if (obj == null) return true

  // Assume if it has a length property with a non-zero value
  // that that property is correct.
  if (obj.length > 0) return false
  if (obj.length === 0) return true

  // If it isn't an object at this point
  // it is empty, but it can't be anything *but* empty.
  if (typeof obj !== 'object') return true

  // Otherwise, does it have any properties of its own?
  for (var key in obj) {
    if (hasOwnProperty.call(obj, key)) return false
  }

  return true
}
/**
 * Update depp object structure and cann be append Arrays
 * @param {Object} current
 * @param {Object} update
 * @param {Boolean} append
 */
function updateObject (current, update, append) {
  append = append || true
  if (isEmpty(current) && !isEmpty(update)) {
    return update
  }
  for (var prop in update) {
    try {
      if (update[prop].constructor === Object) {
        current[prop] = updateObject(current[prop], update[prop])
      } else if (current.hasOwnProperty(prop)) {
        if (update[prop] instanceof Array) {
          if (current[prop] instanceof Array && append) {
            update[prop].forEach(element => {
              current[prop].push(element)
            })
          } else {
            current[prop] = update[prop]
          }
        } else {
          current[prop] = update[prop]
        }
      } else {
        current[prop] = update[prop]
      }
    } catch (e) {
      current[prop] = update[prop]
    }
  }
  return current
}

function isInt (n) {
  return Number(n) === n && n % 1 === 0
}

function isFloat (n) {
  return Number(n) === n && n % 1 !== 0
}
