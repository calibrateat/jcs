# JSON Condition Service (JCS)
This module use the mongo query language to check conditions. Here we use the mingo module.
## Income data structure
( see (http://calibrate.at/schema/condition.json) )
Property | Type | Default | Description
---------|------|---------|------------
setting | Object | see setting | controll of functionality (optional)
data | Object | see data | the data base for all condition
global | Object | null | start values or null conditon out (optional)
conditions | Array | … | …
## Simple Usage
## Usage
```javascript
const jcs = require('jcs')

// simple
let simple = jcs({'conditions':[…], 'data':{…}})
let result = jcs(require('./condition_sample.json'), require('./condition_data_sample.json'))

console.log(JSON.stringify(result))

```
## Files
* condition from type json (http://calibrate.at/schema/condition.json)
* data to use with conditon
* or one file with both structure see data below
## Data
Data can be written a seperately json file and will be the second argument of jcs. Or simple in the condition file in data as Object.
### Structure income data object
Property | Type | Description
---------|------|-------------
setting | object | see setting below (optional)
conditions | arrays | see below
global | object | default data.___result (optional)
data | object | value base for resolving (optional)
data.__calc | object | value base for resolving, but can be also resolved (optional)
## Setting
Property | Type | Default | Description
---------|------|---------|------------
digits | number | 2 | if a calc this is round setting
onlyFirst | boolean | false | if true first match will only response  
first2last | boolean | true | work from first to last constion or reverse
placeholder | boolean | false | if true resolve placeholder before iterate
stepResolver | boolean | false | if true after every step resolve with result
debug | boolean | false | if true results are saved in a collection on result
append | boolean | false | if true response the data income object with apended result, default give only result back
## PlaceHolder
### Calc (only if placeholder enabled)
Property | Type | Description
---------|------|-------------
sum | array | …
sub | array | …
mul | array | …
div | array | …
mod | array | …
abs | object | …
#### Calc sample
```json
 "value": {"$calc":{"sub":["#/productList/0/intent/layoutIntent/dimensions/0", 6 ]}}
```
### Structure
Property | Type | Description
---------|------|-------------
$goToId | string | target condition (no return back to start index)
$goBack | boolean | in a subcondition say true go back to the start of subconditon
$break | boolean | if true stop conditon worker
$subCondition | boolean | default false, use to ignore conditon, by $goToId such as
$log | string | log the given string 
$if | object | if condition 
$digits | number | in a $calc round digits
$setID | string | bracket over conditions, ignored by not set flow
subCondition | boolean | says it is a subcondion if is true
