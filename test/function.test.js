/* eslint-env mocha */

'use strict'

require('./common')

describe('Function-Test', function () {
  it('merge', function () {
    let mergeCondition = require('./lib/merge_condition.json')
    let mergeData = require('./lib/merge_data.json')
    let mergeResult = require('./lib/merge_result.json')
    return Promise.resolve(jcs(mergeCondition, mergeData))
      .then(res => {
        expect(res).to.be.a('object')
        expect(res).to.eql(mergeResult)
      })
  })
  it('jobTicket test', function () {
    let jobTicketCondition = require('./lib/jobticket_condition.json')
    let jobTicketData = require('./lib/jobticket_data.json')
    let jobTicketResult = require('./lib/jobticket_result.json')
    return Promise.resolve(jcs(jobTicketCondition, jobTicketData))
      .then(res => {
        console.log(JSON.stringify(res))
        expect(res).to.be.a('object')
        expect(res).to.eql(jobTicketResult)
      })
  })
  /* it('goToId', () => {

  })
  it('goBack', () => {

  })
  it('break', () => {

  })
  it('subCondition', () => {

  })
  it('if', () => {

  }) */
})
