/* eslint no-template-curly-in-string: "off" */
/* eslint no-useless-escape: "off" */
/* eslint no-eval: "off" */

const refResolver = require('referenz-resolver')

// test
const valueJson = {
  'foo': {
    'bar': true,
    'vug': 23,
    'her': 12,
    'gss': 'Putin'
  },
  'geertz': 389.56,
  'heertz': 12.89,
  'jeertz': -23.89,
  'zuitz': 'Wilhelm',
  'uirtz': 'Clara',
  'deesu': false,
  'fooBar': [
    { 'aaa': 23, 'bbb': 33, 'ccc': 'Mandy' },
    42,
    false,
    89
  ]
}

let _resolved = refResolver(
  { foo: '#/foo/her', bar: '#/fooBar/0/bbb' },
  valueJson
)
console.log(_resolved)

_resolved = refResolver(
  { sum: { $sum: ['#/foo/her', '#/foo/vug', { $sum: [10, 5] }] } },
  valueJson
)
console.log(JSON.stringify(_resolved))

_resolved = refResolver(
  { sum: { $sub: ['#/foo/her', '#/foo/vug', { $sum: [10, 5] }] } },
  valueJson
)
console.log(JSON.stringify(_resolved))

_resolved = refResolver(
  { sum: { $mul: ['#/foo/her', '#/foo/vug', { $substract: [10, 5] }] } },
  valueJson
)
console.log(JSON.stringify(_resolved))

_resolved = refResolver(
  { sum: { $divide: ['#/foo/her', '#/foo/vug', { $substract: [10, 5] }] } },
  valueJson
)
console.log(JSON.stringify(_resolved))

_resolved = refResolver(
  {
    president: '${foo.gss}',
    foo: '${foo.her * foo.vug|boolean}',
    bar: '${fooBar[0].bbb|number}'
  },
  valueJson
)
console.log(_resolved)

_resolved = refResolver(
  { president: '${"In russia " + foo.gss + ", in germany was it " + zuitz + "."}' },
  valueJson
)
console.log(_resolved)

_resolved = refResolver({ foo: '${17*Math.abs(-23) / foo.her}' }, valueJson)
console.log(_resolved)
