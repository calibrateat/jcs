/* eslint-env mocha */

'use strict'

require('./common')
const resolver = require('referenz-resolver')
const expect = require('chai').expect

describe('Resolver-Test', function () {
  it('resolve', () => {
    let resolveSample = require('./lib/resolve_sample.json')
    let resolveData = require('./lib/resolve_data.json')
    let resolveResult = require('./lib/resolve_result.json')
    return Promise.resolve(resolver(resolveSample, resolveData))
      .then(res => {
        expect(res).to.be.a('object')
        expect(res).to.eql(resolveResult)
      })
  })
  it('calc', () => {
    let resolveSample = require('./lib/calc_resolve_sample.json')
    let resolveData = require('./lib/calc_resolve_data.json')
    let resolveResult = require('./lib/calc_resolve_result.json')
    return Promise.resolve(resolver(resolveSample, resolveData))
      .then(res => {
        expect(res).to.eql(resolveResult)
      })
  })
})
